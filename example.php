<?php

// Note: The namespace is required since a 'log' function already exists in PHP
// and that can't be overriden without having an extension installed, so we
// use a namespace which allows us to use the function name. 

namespace app;

const console = '';

function log(...$args) {
    var_dump(...$args);
    return '';
}

console.log('thingA', 5, [1,2]);

//  Outputs:
// test.php:16:
//  string(6) "thingA"
// test.php:16:
// int(5)
// test.php:16:
// array(2) {
//     [0] =>
// int(1)
// [1] =>
// int(2)
// }